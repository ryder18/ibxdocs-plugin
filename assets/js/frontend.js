+(function($) {

    var pending_ajax = false;

    var ibxDocs = {
        initialize: function() {
            $('.ibxDocs-feedback-wrap').on('click', 'a', this.feedback);
            $('#top-search-form .dropdown-menu').on('click', 'a', this.searchForm);
            $('a.ibxDocs-print-article').on('click', this.printArticle);

            // modal
            $('a#ibxDocs-stuck-modal').on('click', this.showModal);
            $('a#ibxDocs-modal-close').on('click', this.closeModal);
            $('#ibxDocs-modal-backdrop').on('click', this.closeModal);
            $('form#ibxDocs-contact-modal-form').on('submit', this.contactHelp);
        },

        feedback: function(e) {
            e.preventDefault();

            // return if any request is in process already
            if ( pending_ajax ) {
                return;
            }

            pending_ajax = true;

            var self = $(this),
                wrap = self.closest('.ibxDocs-feedback-wrap'),
                data = {
                    post_id: self.data('id'),
                    type: self.data('type'),
                    action: 'ibxDocs_ajax_feedback',
                    _wpnonce: ibxDocs_Vars.nonce
                };

            wrap.append('&nbsp; <i class="ibxDocs-icon ibxDocs-icon-refresh ibxDocs-icon-spin"></i>');
            $.post(ibxDocs_Vars.ajaxurl, data, function(resp) {
                wrap.html(resp.data);

                pending_ajax = false;
            });
        },

        searchForm: function(e) {
            e.preventDefault();

            var param = $(this).attr("href").replace("#","");
            var concept = $(this).text();

            $('#top-search-form span#search_concept').text(concept);
            $('.input-group #search_param').val(param);
        },

        printArticle: function(e) {
            e.preventDefault();

            var article = $(this).closest('article');

            var mywindow = window.open('', 'my div', 'height=600,width=800');
            mywindow.document.write('<html><head><title>Print Article</title>');
            mywindow.document.write('<link rel="stylesheet" href="' + ibxDocs_Vars.style + '" type="text/css" media="all" />');
            mywindow.document.write('</head><body >');
            mywindow.document.write(article.html());
            mywindow.document.write('<div class="powered-by">' + ibxDocs_Vars.powered + '</div>');
            mywindow.document.write('</body></html>');

            mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10

            setTimeout(function() {
                mywindow.print();
                mywindow.close();
            }, 2000);

            return true;
        },

        showModal: function(e) {
            e.preventDefault();

            $('#ibxDocs-modal-backdrop').show();
            $('#ibxDocs-contact-modal').show();
            $('body').addClass('ibxDocs-overflow-hidden');
        },

        closeModal: function(e) {
            e.preventDefault();

            $('#ibxDocs-modal-backdrop').hide();
            $('#ibxDocs-contact-modal').hide();
            $('body').removeClass('ibxDocs-overflow-hidden');
        },

        contactHelp: function(e) {
            e.preventDefault();

            var self = $(this),
                submit = self.find('input[type=submit]'),
                body = self.closest('.ibxDocs-modal-body'),
                data = self.serialize() + '&_wpnonce=' + ibxDocs_Vars.nonce;

            submit.prop('disabled', true);

            $.post(ibxDocs_Vars.ajaxurl, data, function(resp) {
                if ( resp.success === false ) {
                    submit.prop('disabled', false);
                    $('#ibxDocs-modal-errors', body).empty().append('<div class="ibxDocs-alert ibxDocs-alert-danger">' + resp.data + '</div>')
                } else {
                    body.empty().append( '<div class="ibxDocs-alert ibxDocs-alert-success">' + resp.data + '</div>' );
                }
            });
        }
    };

    $(function() {
        ibxDocs.initialize();
    });

    // initialize anchor.js
    anchors.options = {
        icon: '#'
    };
    anchors.add('.ibxDocs-single-content .entry-content > h2, .ibxDocs-single-content .entry-content > h3')

})(jQuery);
