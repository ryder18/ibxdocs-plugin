<?php

function ibxDocs_register_customizer_setttings ( $wp_customize ) {
    // Add IBX Docs section to customize
    $wp_customize->add_panel( 'ibxDocs', array(
        'title' => __( 'IBX Docs' ),
        'description' => $description, // Include html tags such as <p>.
        'priority' => 160, // Mixed with top-level-section hierarchy.
      ) );

    // Add Docs Home sub-section
    $wp_customize->add_section( 'docs_home' , array(
        'title' => 'Docs Home',
        'panel' => 'ibxDocs',
      ) );

    $wp_customize->add_setting(
        'tcx_link_color',
        array(
            'default'     => '#000000',
            'transport'   => 'postMessage'
        )
    );
 
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'link_color',
            array(
                'label'      => __( 'Link Color', 'tcx' ),
                'section'    => 'docs_home',
                'settings'   => 'tcx_link_color'
            )
        )
    );

    $wp_customize->add_setting(
        'ibxdocs_home_layout',
        array(
            "default"   =>  "Use get options.",
            'transport' =>  "postMessage"
        )
    );

    $wp_customize->add_control(
        'home_layout',
        array(
            'type'      =>  'select',
            'prioriy'   =>  10,
            'section'   =>  'docs_home',
            'label'     =>  __('Layout', 'ibxdocs'),
            'options'   =>  array('grid-1'  => 'Grid 1'),
        )
    );
}
add_action( 'customize_register', 'ibxDocs_register_customizer_setttings' );

function tcx_customizer_css() {
    ?>
    <style type="text/css">
        a { color: <?php echo get_theme_mod( 'tcx_link_color' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'tcx_customizer_css' );

function tcx_customizer_live_preview() {
 
    wp_enqueue_script(
        'tcx-theme-customizer',
        IBX_DOCS_URL . 'assets/js/ibx-customizer.js',
        array( 'jquery', 'customize-preview' ),
        IBX_DOCS_VER
    );
 
}
add_action( 'customize_preview_init', 'tcx_customizer_live_preview' );

?>