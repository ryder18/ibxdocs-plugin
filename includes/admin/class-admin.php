<?php

/**
 * Admin Class
 */
class ibxDocs_Admin {

    function __construct() {
        $this->includes();
        $this->init_actions();
        $this->init_classes();
    }

    public function includes() {
        require_once dirname( dirname( __FILE__ ) ) . '/class-settings-api.php';
        require_once dirname( __FILE__ ) . '/class-settings.php';
        require_once dirname( __FILE__ ) . '/class-docs-list-table.php';
    }

    /**
     * Initialize action hooks
     *
     * @return void
     */
    public function init_actions() {
        add_action( 'admin_enqueue_scripts', array( $this, 'admin_scripts' ) );

        add_action( 'admin_menu', array( $this, 'admin_menu' ) );
        add_filter( 'parent_file', array($this, 'fix_tag_menu' ) );

        add_filter( 'admin_footer_text', array( $this, 'admin_footer_text' ), 1 );
    }

    public function init_classes() {
        new ibxDocs_Settings();
        new ibxDocs_Docs_List_Table();
    }

    /**
     * Load admin scripts and styles
     *
     * @param  string
     *
     * @return void
     */
    public function admin_scripts( $hook ) {
        if ( 'toplevel_page_ibxDocs' != $hook ) {
            return;
        }

        $suffix     = ( defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ) ? '' : '.min';
        $assets_url = ibxDocs()->plugin_url() . '/assets';

        wp_enqueue_script( 'vuejs', $assets_url . '/js/vue' . $suffix . '.js' );
        wp_enqueue_script( 'sweetalert', $assets_url . '/js/sweetalert.min.js', array( 'jquery' ) );
        wp_enqueue_script( 'ibxDocs-admin-script', $assets_url . "/js/admin-script.js", array( 'jquery', 'jquery-ui-sortable', 'wp-util' ), time(), true );
        wp_localize_script( 'ibxDocs-admin-script', 'ibxDocs', array(
            'nonce'                 => wp_create_nonce( 'ibxDocs-admin-nonce' ),
            'editurl'               => admin_url( 'post.php?action=edit&post=' ),
            'viewurl'               => home_url( '/?p=' ),
            'enter_doc_title'       => __( 'Enter doc title', 'ibxDocs' ),
            'write_something'       => __( 'Write something', 'ibxDocs' ),
            'enter_section_title'   => __( 'Enter section title', 'ibxDocs' ),

        ) );

        wp_enqueue_style( 'sweetalert', $assets_url . '/css/sweetalert.css', false, date( 'Ymd' ) );
        wp_enqueue_style( 'ibxDocs-admin-styles', $assets_url . '/css/admin.css', false, date( 'Ymd' ) );
    }

    /**
     * Get the admin menu position
     *
     * @return int the position of the menu
     */
    public function get_menu_position() {
        return apply_filters( 'ibxDocs_menu_position', 48 );
    }

    /**
     * Add menu items
     *
     * @return void
     */
    public function admin_menu() {
        $capability = ibxDocs_get_publish_cap();

        add_menu_page( __( 'ibxDocs', 'ibxDocs' ), __( 'ibxDocs', 'ibxDocs' ), $capability, 'ibxDocs', array( $this, 'page_index' ), 'dashicons-media-document', $this->get_menu_position() );
        add_submenu_page( 'ibxDocs', __( 'Docs', 'ibxDocs' ), __( 'Docs', 'ibxDocs' ), $capability, 'ibxDocs', array( $this, 'page_index' ) );
        add_submenu_page( 'ibxDocs', __( 'Tags', 'ibxDocs' ), __( 'Tags', 'ibxDocs' ), 'manage_categories', 'edit-tags.php?taxonomy=doc_tag&post_type=docs' );
    }

    /**
     * highlight the proper top level menu
     *
     * @link http://wordpress.org/support/topic/moving-taxonomy-ui-to-another-main-menu?replies=5#post-2432769
     *
     * @global obj $current_screen
     * @param string $parent_file
     *
     * @return string
     */
    function fix_tag_menu( $parent_file ) {
        global $current_screen;

        if ( $current_screen->taxonomy == 'doc_tag' || $current_screen->post_type == 'docs' ) {
            $parent_file = 'ibxDocs';
        }

        return $parent_file;
    }

    /**
     * UI Page handler
     *
     * @return void
     */
    public function page_index() {
        include dirname( __FILE__ ) . '/template-vue.php';
    }

    /**
     * Change the admin footer text on ibxDocs admin pages
     *
     * @param  string $footer_text
     * @return string
     */
    public function admin_footer_text( $footer_text ) {
        // if ( ! current_user_can( ibxDocs_get_publish_cap() ) ) {
        //     return;
        // }

        $footer_text    = '';
        $current_screen = get_current_screen();
        $pages          = array( 'toplevel_page_ibxDocs', 'edit-docs' );

        // Check to make sure we're on a ibxDocs admin page
        if ( isset( $current_screen->id ) && apply_filters( 'ibxDocs_display_admin_footer_text', in_array( $current_screen->id, $pages ) ) ) {

            if ( ! get_option( 'ibxDocs_admin_footer_text_rated' ) ) {
                $footer_text .= ' ' . sprintf( __( 'Thank you for using <strong>ibxDocs</strong>. Please leave us a %s&#9733;&#9733;&#9733;&#9733;&#9733;%s rating.', 'ibxDocs' ), '<a href="https://wordpress.org/support/view/plugin-reviews/ibxDocs?filter=5#postform" target="_blank" class="ibxDocs-rating-link" data-rated="' . esc_attr__( 'Thanks :)', 'ibxDocs' ) . '">', '</a>' );
                $script = '<script type="text/javascript">
                    jQuery(function($){$( "a.ibxDocs-rating-link" ).click( function() {
                        $.post( ajaxurl, { action: "ibxDocs_rated", _wpnonce: ibxDocs.nonce } );
                        $( this ).parent().text( $( this ).data( "rated" ) );
                    });});
                    </script>
                ';

                echo $script;
            } else {
                $footer_text .= ' ' . __( 'Thank you for using <strong>ibxDocs</strong>.', 'ibxDocs' );
            }

            $footer_text .= ' ' . sprintf( __( 'Use the <a href="%s">classic UI</a>.', 'ibxDocs' ), admin_url( 'edit.php?post_type=docs' ) );
        }

        return $footer_text;
    }

}
