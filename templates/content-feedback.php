<?php global $post; ?>

<div class="ibxDocs-feedback-wrap ibxDocs-hide-print">
    <?php
    $positive = (int) get_post_meta( $post->ID, 'positive', true );
    $negative = (int) get_post_meta( $post->ID, 'negative', true );

    $positive_title = $positive ? sprintf( _n( '%d person found this useful', '%d persons found this useful', $positive, 'ibxDocs' ), number_format_i18n( $positive ) ) : __( 'No votes yet', 'ibxDocs' );
    $negative_title = $negative ? sprintf( _n( '%d person found this not useful', '%d persons found this not useful', $negative, 'ibxDocs' ), number_format_i18n( $negative ) ) : __( 'No votes yet', 'ibxDocs' );
    ?>

    <?php _e( 'Was this article helpful to you?', 'ibxDocs' ); ?>

    <span class="vote-link-wrap">
        <a href="#" class="ibxDocs-tip positive" data-id="<?php the_ID(); ?>" data-type="positive" title="<?php echo esc_attr( $positive_title ); ?>">
            <?php _e( 'Yes', 'ibxDocs' ); ?>

            <?php if ( $positive ) { ?>
                <span class="count"><?php echo number_format_i18n( $positive ); ?></span>
            <?php } ?>
        </a>
        <a href="#" class="ibxDocs-tip negative" data-id="<?php the_ID(); ?>" data-type="negative" title="<?php echo esc_attr( $negative_title ); ?>">
            <?php _e( 'No', 'ibxDocs' ); ?>

            <?php if ( $negative ) { ?>
                <span class="count"><?php echo number_format_i18n( $negative ); ?></span>
            <?php } ?>
        </a>
    </span>
</div>