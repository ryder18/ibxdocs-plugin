<div class="ibxDocs-shortcode-wrap ibxDocs-list-1">
    		<ul class="ibxDocs-docs-list">
				<?php foreach ($docs as $main_doc) : ?>
				<?php 
					/* Check if Doc has any sections or not */

					if ( $main_doc['sections'] ) : ?>

           			<li class="ibxDocs-docs-single">
					<div class="ibxDocs-docs-single-tile-section-wrapper">		
						<h3 class="ibxDocs-docs-single-title">
							<a href="<?php echo get_permalink( $main_doc['doc']->ID ); ?>"><?php echo $main_doc['doc']->post_title; ?></a>
						</h3>

						<div class="ibxDocs-doc-content">
							<ul class="ibxDocs-doc-items">
								<?php $i= 1; $count = count($main_doc['sections']); foreach ($main_doc['sections'] as $section) : ?>
									<li>
										<a class="ibxDocs-doc-item" href="<?php  echo get_permalink( $section->ID ); ?>">
											<?php 
												if ( $i < $count ) {
													echo $section->post_title.",";
												} else {
													echo $section->post_title.".";
												}
												$i++;
											?>
										</a>
									</li>
								<?php endforeach; ?>
							</ul>
						</div>

					</div>
					<span class="ibxDocs-docs-single-topic-count">
							<?php echo count($main_doc['sections']); ?>
						</span>
						
					<?php endif; ?>				
				<?php

				/**
				 * Hide Read More section if the parameter contains 'none' else it will show
				 * the set Read More text.
				 */
				
                    if("none" !== $more) {
                ?>
						<div class="ibxDocs-doc-link">
							<a href="<?php echo get_permalink( $main_doc['doc']->ID ); ?>">
								<?php echo $more; ?>
							</a>
						</div>
                    </li>
                <?php } endforeach; ?>
			</ul>
        </div>