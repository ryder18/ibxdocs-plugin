<div class="ibxDocs-shortcode-wrap ibxDocs-grid">
    <ul class="ibxDocs-docs-list col-<?php echo $col; ?>">

        <?php foreach ($docs as $main_doc) : ?>
            <li class="ibxDocs-docs-single">
                <h3 class="ibxDocs-docs-single-title"><a href="<?php echo get_permalink( $main_doc['doc']->ID ); ?>"><?php echo $main_doc['doc']->post_title; ?></a></h3>

                <?php if ( $main_doc['sections'] ) : ?>

                    <div class="ibxDocs-doc-content">
                        <ul class="ibxDocs-doc-items">
                            <?php foreach ($main_doc['sections'] as $section) : ?>
                                <li><a href="<?php echo get_permalink( $section->ID ); ?>"><?php echo $section->post_title; ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>

                <?php endif; 
                    if("none" !== $more){
                ?>
                    <div class="ibxDocs-doc-link">
                    <a href="<?php echo get_permalink( $main_doc['doc']->ID ); ?>"><?php echo $more; ?></a>
                </div>
            </li>
        <?php } endforeach; ?>
    </ul>
</div>