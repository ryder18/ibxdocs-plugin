<?php
$name = $email = '';

if ( is_user_logged_in() ) {
    $user  = wp_get_current_user();
    $name  = $user->display_name;
    $email = $user->user_email;
}
?>

<div class="ibxDocs-modal-backdrop" id="ibxDocs-modal-backdrop"></div>
<div id="ibxDocs-contact-modal" class="ibxDocs-contact-modal ibxDocs-hide-print">
    <div class="ibxDocs-modal-header">
        <h1><?php _e( 'How can we help?', 'ibxDocs' ); ?></h1>
        <a href="#" id="ibxDocs-modal-close" class="ibxDocs-modal-close"><i class="ibxDocs-icon ibxDocs-icon-times"></i></a>
    </div>

    <div class="ibxDocs-modal-body">
        <div id="ibxDocs-modal-errors"></div>
        <form id="ibxDocs-contact-modal-form" action="" method="post">
            <div class="ibxDocs-form-row">
                <label for="name"><?php _e( 'Name', 'ibxDocs' ); ?></label>

                <div class="ibxDocs-form-field">
                    <input type="text" name="name" id="name" placeholder="" value="<?php echo $name; ?>" required />
                </div>
            </div>

            <div class="ibxDocs-form-row">
                <label for="name"><?php _e( 'Email', 'ibxDocs' ); ?></label>

                <div class="ibxDocs-form-field">
                    <input type="email" name="email" id="email" placeholder="you@example.com" value="<?php echo $email; ?>" <?php disabled( is_user_logged_in() ); ?> required />
                </div>
            </div>

            <div class="ibxDocs-form-row">
                <label for="name"><?php _e( 'subject', 'ibxDocs' ); ?></label>

                <div class="ibxDocs-form-field">
                    <input type="text" name="subject" id="subject" placeholder="" value="" required />
                </div>
            </div>

            <div class="ibxDocs-form-row">
                <label for="name"><?php _e( 'message', 'ibxDocs' ); ?></label>

                <div class="ibxDocs-form-field">
                    <textarea type="message" name="message" id="message" required></textarea>
                </div>
            </div>

            <div class="ibxDocs-form-action">
                <input type="submit" name="submit" value="<?php echo esc_attr_e( 'Send', 'ibxDocs' ); ?>">
                <input type="hidden" name="doc_id" value="<?php the_ID(); ?>">
                <input type="hidden" name="action" value="ibxDocs_contact_feedback">
            </div>
        </form>
    </div>
</div>