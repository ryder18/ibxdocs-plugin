<?php
/**
 * The template for displaying a single doc
 *
 * To customize this template, create a folder in your current theme named "ibxDocs" and copy it there.
 *
 * @package ibxDocs
 */

$skip_sidebar = ( get_post_meta( $post->ID, 'skip_sidebar', true ) == 'yes' ) ? true : false;

get_header(); ?>

    <?php
        /**
         * @since 1.4
         *
         * @hooked ibxDocs_template_wrapper_start - 10
         */
        do_action( 'ibxDocs_before_main_content' );
    ?>

    <?php while ( have_posts() ) : the_post(); ?>

        <div class="ibxDocs-single-wrap">

            <?php if ( ! $skip_sidebar ) { ?>

                <?php ibxDocs_get_template_part( 'docs', 'sidebar' ); ?>

            <?php } ?>

            <div class="ibxDocs-single-content">
                <?php ibxDocs_breadcrumbs(); ?>

                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemscope itemtype="http://schema.org/Article">
                    <header class="entry-header">
                        <?php the_title( '<h1 class="entry-title" itemprop="headline">', '</h1>' ); ?>

                        <?php if ( ibxDocs_get_option( 'print', 'ibxDocs_settings', 'on' ) == 'on' ): ?>
                            <a href="#" class="ibxDocs-print-article ibxDocs-hide-print ibxDocs-hide-mobile" title="<?php echo esc_attr( __( 'Print this article', 'ibxDocs' ) ); ?>"><i class="ibxDocs-icon ibxDocs-icon-print"></i></a>
                        <?php endif; ?>
                    </header><!-- .entry-header -->

                    <div class="entry-content" itemprop="articleBody">
                        <?php
                            the_content( sprintf(
                                /* translators: %s: Name of current post. */
                                wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'ibxDocs' ), array( 'span' => array( 'class' => array() ) ) ),
                                the_title( '<span class="screen-reader-text">"', '"</span>', false )
                            ) );

                            wp_link_pages( array(
                                'before' => '<div class="page-links">' . esc_html__( 'Docs:', 'ibxDocs' ),
                                'after'  => '</div>',
                            ) );

                            $children = wp_list_pages("title_li=&order=menu_order&child_of=". $post->ID ."&echo=0&post_type=" . $post->post_type);

                            if ( $children ) {
                                echo '<div class="article-child well">';
                                    echo '<h3>' . __( 'Articles', 'ibxDocs' ) . '</h3>';
                                    echo '<ul>';
                                        echo $children;
                                    echo '</ul>';
                                echo '</div>';
                            }

                            $tags_list = ibxDocs_get_the_doc_tags( $post->ID, '', ', ' );

                            if ( $tags_list ) {
                                printf( '<span class="tags-links"><span class="screen-reader-text">%1$s </span>%2$s</span>',
                                    _x( 'Tags', 'Used before tag names.', 'ibxDocs' ),
                                    $tags_list
                                );
                            }
                        ?>
                    </div><!-- .entry-content -->

                    <footer class="entry-footer ibxDocs-entry-footer">
                        <?php if ( ibxDocs_get_option( 'email', 'ibxDocs_settings', 'on' ) == 'on' ): ?>
                            <span class="ibxDocs-help-link ibxDocs-hide-print ibxDocs-hide-mobile">
                                <i class="ibxDocs-icon ibxDocs-icon-envelope"></i>
                                <?php printf( '%s <a id="ibxDocs-stuck-modal" href="%s">%s</a>', __( 'Still stuck?', 'ibxDocs' ), '#', __( 'How can we help?', 'ibxDocs' ) ); ?>
                            </span>
                        <?php endif; ?>

                        <div class="ibxDocs-article-author" itemprop="author" itemscope itemtype="https://schema.org/Person">
                            <meta itemprop="name" content="<?php echo get_the_author(); ?>" />
                            <meta itemprop="url" content="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" />
                        </div>

                        <meta itemprop="datePublished" content="<?php echo get_the_time( 'c' ); ?>"/>
                        <time itemprop="dateModified" datetime="<?php echo esc_attr( get_the_modified_date( 'c' ) ); ?>"><?php printf( __( 'Updated on %s', 'ibxDocs' ), get_the_modified_date() ); ?></time>
                    </footer>

                    <?php ibxDocs_doc_nav(); ?>

                    <?php if ( ibxDocs_get_option( 'helpful', 'ibxDocs_settings', 'on' ) == 'on' ): ?>
                        <?php ibxDocs_get_template_part( 'content', 'feedback' ); ?>
                    <?php endif; ?>

                    <?php if ( ibxDocs_get_option( 'email', 'ibxDocs_settings', 'on' ) == 'on' ): ?>
                        <?php ibxDocs_get_template_part( 'content', 'modal' ); ?>
                    <?php endif; ?>

                    <?php if ( ibxDocs_get_option( 'comments', 'ibxDocs_settings', 'off' ) == 'on' ): ?>
                        <?php if ( comments_open() || get_comments_number() ) : ?>
                            <div class="ibxDocs-comments-wrap">
                                <?php comments_template(); ?>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>

                </article><!-- #post-## -->
            </div><!-- .ibxDocs-single-content -->
        </div><!-- .ibxDocs-single-wrap -->

    <?php endwhile; ?>

    <?php
        /**
         * @since 1.4
         *
         * @hooked ibxDocs_template_wrapper_end - 10
         */
        do_action( 'ibxDocs_after_main_content' );
    ?>

<?php get_footer(); ?>
