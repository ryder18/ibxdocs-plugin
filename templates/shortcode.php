<?php if ( $docs ) : 

	switch($layout) {
		case "grid-1":
			include plugin_dir_path(__FILE__)."layouts/grid/grid-1.php";
			break;

		case "list-1":
			include plugin_dir_path(__FILE__)."layouts/list/list-1.php";
			break;

		case "list-2":
			include plugin_dir_path(__FILE__)."layouts/list/list-2.php";
			break;
			
		case "list-3":
		include plugin_dir_path(__FILE__)."layouts/list/list-3.php";
		break;

		default:
			echo "Please select a layout.";		
	}
	
endif;