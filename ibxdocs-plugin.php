<?php
/*
Plugin Name: ibxDocs
Plugin URI: http://wedevs.com/
Description: A documentation plugin for WordPress
Version: 1.5
Author: Tareq Hasan
Author URI: https://tareq.co/
License: GPL2
Text Domain: ibxDocs
Domain Path: /languages
*/

/**
 * Copyright (c) 2019 Tareq Hasan (email: tareq@wedevs.com). All rights reserved.
 *
 * Released under the GPL license
 * http://www.opensource.org/licenses/gpl-license.php
 *
 * This is an add-on for WordPress
 * http://wordpress.org/
 *
 * **********************************************************************
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * **********************************************************************
 */

// don't call the file directly
if ( !defined( 'ABSPATH' ) ) exit;

define( 'IBX_DOCS_VER', '0.0.1' );
define( 'IBX_DOCS_DIR', plugin_dir_path( __FILE__ ) );
define( 'IBX_DOCS_URL', plugins_url( '/', __FILE__ ) );
define( 'IBX_DOCS_PATH', plugin_basename( __FILE__ ) );
define( 'IBX_DOCS_FILE', __FILE__ );

/**
 * ibxDocs class
 *
 * @class ibxDocs The class that holds the entire ibxDocs plugin
 */
class ibxDocs {

    /**
     * Plugin version
     *
     * @var string
     */
    public $version = '1.4.1';

    /**
     * The plugin url
     *
     * @var string
     */
    public $plugin_url;

    /**
     * The plugin path
     *
     * @var string
     */
    public $plugin_path;

    /**
     * The theme directory path
     *
     * @var string
     */
    public $theme_dir_path;

    /**
     * The post type name
     *
     * @var string
     */
    private $post_type = 'docs';

    /**
     * Initializes the ibxDocs() class
     *
     * Checks for an existing ibxDocs() instance
     * and if it doesn't find one, creates it.
     */
    public static function init() {
        static $instance = false;

        if ( ! $instance ) {
            $instance = new ibxDocs();

            add_action('after_setup_theme', array($instance, 'plugin_init'));

            register_activation_hook( __FILE__, array($instance, 'activate' ) );
        }

        return $instance;
    }

    /**
     * Initialize the plugin
     *
     * @return void
     */
    function plugin_init() {
        $this->theme_dir_path = apply_filters( 'ibxDocs_theme_dir_path', 'ibxDocs/' );

        $this->file_includes();
        $this->init_classes();

        // Localize our plugin
        add_action( 'init', array( $this, 'localization_setup' ) );

        // custom post types and taxonomies
        add_action( 'init', array( $this, 'register_post_type' ) );
        add_action( 'init', array( $this, 'register_taxonomy' ) );

        // filter the search result
        add_action( 'pre_get_posts', array( $this, 'docs_search_filter' ) );

        // registeer our widget
        add_action( 'widgets_init', array( $this, 'register_widget' ) );

        // override the theme template
        add_filter( 'template_include', array( $this, 'template_loader' ), 20 );

        // Loads frontend scripts and styles
        add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

        add_action( 'rest_api_init', array( $this, 'init_rest_api' ) );
    }

    /**
     * The plugin activation function
     *
     * @since 1.3
     *
     * @return void
     */
    public function activate() {

        $this->maybe_create_docs_page();

        // rewrite rules problem, register and flush
        $this->register_post_type();
        $this->register_taxonomy();

        flush_rewrite_rules();

        update_option( 'ibxDocs_installed', time() );
        update_option( 'ibxDocs_version', $this->version );
    }

    /**
     * Load the required files
     *
     * @return void
     */
    function file_includes() {
        include_once dirname( __FILE__ ) . '/includes/functions.php';
        include_once dirname( __FILE__ ) . '/includes/class-walker-docs.php';
        include_once dirname( __FILE__ ) . '/includes/class-search-widget.php';
        include_once dirname( __FILE__ ) . '/includes/class-theme-support.php';
        include_once dirname( __FILE__ ) . '/includes/rest-api/class-rest-api.php';
        include_once dirname( __FILE__ ) . '/includes/customizer.php';

        if ( is_admin() ) {
            include_once dirname( __FILE__ ) . '/includes/admin/class-admin.php';
        } else {
            include_once dirname( __FILE__ ) . '/includes/class-shortcode.php';
        }

        if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
            include_once dirname( __FILE__ ) . '/includes/class-ajax.php';
        }
    }

    /**
     * Initialize the classes
     *
     * @since 1.4
     *
     * @return void
     */
    public function init_classes() {

        new ibxDocs_Theme_Support();

        if ( is_admin() ) {
            new ibxDocs_Admin();
        } else {
            new ibxDocs_Shortcode_Handler();
        }

        if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
            new ibxDocs_Ajax();
        }
    }

    /**
     * Initialize REST API
     *
     * @return void
     */
    public function init_rest_api() {
        $api = new ibxDocs_REST_API();
        $api->register_routes();
    }

    /**
     * Initialize plugin for localization
     *
     * @uses load_plugin_textdomain()
     */
    public function localization_setup() {
        load_plugin_textdomain( 'ibxDocs', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
    }

    /**
     * Enqueue admin scripts
     *
     * Allows plugin assets to be loaded.
     *
     * @uses wp_enqueue_script()
     * @uses wp_localize_script()
     * @uses wp_enqueue_style
     */
    public function enqueue_scripts() {

        /**
         * All styles goes here
         */
        wp_enqueue_style( 'ibxDocs-styles', plugins_url( 'assets/css/frontend.css', __FILE__ ), array(), $this->version );

        /**
         * All scripts goes here
         */
        wp_enqueue_script( 'ibxDocs-anchorjs', plugins_url( 'assets/js/anchor.min.js', __FILE__ ), array( 'jquery' ), $this->version, true );
        wp_enqueue_script( 'ibxDocs-scripts', plugins_url( 'assets/js/frontend.js', __FILE__ ), array( 'jquery', 'ibxDocs-anchorjs' ), $this->version, true );
        wp_localize_script( 'ibxDocs-scripts', 'ibxDocs_Vars', array(
            'ajaxurl' => admin_url( 'admin-ajax.php' ),
            'nonce'   => wp_create_nonce( 'ibxDocs-ajax' ),
            'style'   => plugins_url( 'assets/css/print.css', __FILE__ ),
            'powered' => sprintf( '&copy; %s, %d. %s<br>%s', get_bloginfo( 'name' ), date( 'Y' ), __( 'Powered by ibxDocs', 'ibxDocs' ), home_url() )
        ) );
    }

    /**
     * Register the post type
     *
     * @return void
     */
    function register_post_type() {

        $labels = array(
            'name'                => _x( 'Docs', 'Post Type General Name', 'ibxDocs' ),
            'singular_name'       => _x( 'Doc', 'Post Type Singular Name', 'ibxDocs' ),
            'menu_name'           => __( 'Documentation', 'ibxDocs' ),
            'parent_item_colon'   => __( 'Parent Doc', 'ibxDocs' ),
            'all_items'           => __( 'All Documentations', 'ibxDocs' ),
            'view_item'           => __( 'View Documentation', 'ibxDocs' ),
            'add_new_item'        => __( 'Add Documentation', 'ibxDocs' ),
            'add_new'             => __( 'Add New', 'ibxDocs' ),
            'edit_item'           => __( 'Edit Documentation', 'ibxDocs' ),
            'update_item'         => __( 'Update Documentation', 'ibxDocs' ),
            'search_items'        => __( 'Search Documentation', 'ibxDocs' ),
            'not_found'           => __( 'Not documentation found', 'ibxDocs' ),
            'not_found_in_trash'  => __( 'Not found in Trash', 'ibxDocs' ),
        );
        $rewrite = array(
            'slug'                => 'docs',
            'with_front'          => true,
            'pages'               => true,
            'feeds'               => true,
        );
        $args = array(
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', 'page-attributes', 'comments' ),
            'hierarchical'        => true,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => false,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'menu_icon'           => 'dashicons-portfolio',
            'can_export'          => true,
            'has_archive'         => false,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'show_in_rest'        => true,
            'rewrite'             => $rewrite,
            'capability_type'     => 'post',
            'taxonomies'          => array( 'doc_tag' )
        );

        register_post_type( $this->post_type, apply_filters( 'ibxDocs_post_type', $args ) );
    }

    /**
     * Register doc tags taxonomy
     *
     * @return void
     */
    function register_taxonomy() {

        $labels = array(
            'name'                       => _x( 'Tags', 'Taxonomy General Name', 'ibxDocs' ),
            'singular_name'              => _x( 'Tag', 'Taxonomy Singular Name', 'ibxDocs' ),
            'menu_name'                  => __( 'Tags', 'ibxDocs' ),
            'all_items'                  => __( 'All Tags', 'ibxDocs' ),
            'parent_item'                => __( 'Parent Tag', 'ibxDocs' ),
            'parent_item_colon'          => __( 'Parent Tag:', 'ibxDocs' ),
            'new_item_name'              => __( 'New Tag', 'ibxDocs' ),
            'add_new_item'               => __( 'Add New Item', 'ibxDocs' ),
            'edit_item'                  => __( 'Edit Tag', 'ibxDocs' ),
            'update_item'                => __( 'Update Tag', 'ibxDocs' ),
            'view_item'                  => __( 'View Tag', 'ibxDocs' ),
            'separate_items_with_commas' => __( 'Separate items with commas', 'ibxDocs' ),
            'add_or_remove_items'        => __( 'Add or remove items', 'ibxDocs' ),
            'choose_from_most_used'      => __( 'Choose from the most used', 'ibxDocs' ),
            'popular_items'              => __( 'Popular Tags', 'ibxDocs' ),
            'search_items'               => __( 'Search Tags', 'ibxDocs' ),
            'not_found'                  => __( 'Not Found', 'ibxDocs' ),
            'no_terms'                   => __( 'No items', 'ibxDocs' ),
            'items_list'                 => __( 'Tags list', 'ibxDocs' ),
            'items_list_navigation'      => __( 'Tags list navigation', 'ibxDocs' ),
        );

        $rewrite = array(
            'slug'                       => 'doc-tag',
            'with_front'                 => true,
            'hierarchical'               => false,
        );

        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => false,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => true,
            'show_in_rest'               => true,
            'rewrite'                    => $rewrite
        );

        register_taxonomy( 'doc_tag', array( 'docs' ), $args );

    }

    /**
     * Register the search widget
     *
     * @return void
     */
    public function register_widget() {
        register_widget( 'ibxDocs_Search_Widget' );
    }

    /**
     * Get the plugin url.
     *
     * @return string
     */
    public function plugin_url() {
        if ( $this->plugin_url ) {
            return $this->plugin_url;
        }

        return $this->plugin_url = untrailingslashit( plugins_url( '/', __FILE__ ) );
    }


    /**
     * Get the plugin path.
     *
     * @return string
     */
    public function plugin_path() {
        if ( $this->plugin_path ) return $this->plugin_path;

        return $this->plugin_path = untrailingslashit( plugin_dir_path( __FILE__ ) );
    }

    /**
     * Get the template path.
     *
     * @return string
     */
    public function template_path() {
        return $this->plugin_path() . '/templates/';
    }

    /**
     * If the theme doesn't have any single doc handler, load that from
     * the plugin
     *
     * @param  string  $template
     *
     * @return string
     */
    function template_loader( $template ) {
        $find = array( $this->post_type . '.php' );
        $file = '';

        if ( is_single() && get_post_type() == $this->post_type ) {
            $file   = 'single-' . $this->post_type . '.php';
            $find[] = $file;
            $find[] = $this->theme_dir_path. $file;
        }

        if ( $file ) {

            $template = locate_template( $find );

            if ( ! $template ) {
                $template = $this->template_path() . $file;
            }
        }

        return $template;
    }

    /**
     * Handle the search filtering in search page
     *
     * @param  \WP_Query  $query
     *
     * @return \WP_Query
     */
    function docs_search_filter( $query ) {

        if ( ! is_admin() && $query->is_main_query() ) {
            if( is_search() ) {
                $param = isset( $_GET['search_in_doc'] ) ? sanitize_text_field( $_GET['search_in_doc'] ) : false;

                if ( $param ) {

                    if ( $param != 'all' ) {
                        $parent_doc_id = intval( $param );
                        $post__in      = array( $parent_doc_id => $parent_doc_id );
                        $children_docs = ibxDocs_get_posts_children( $parent_doc_id, 'docs' );

                        if ( $children_docs ) {
                            $post__in = array_merge( $post__in, wp_list_pluck( $children_docs, 'ID' ) );
                        }

                        $query->set( 'post__in', $post__in );
                    }
                }
            }
        }

        return $query;
    }

    /**
     * Maybe create docs page if not found
     *
     * @since 1.3
     *
     * @return void
     */
    public function maybe_create_docs_page() {
        $version = get_option( 'ibxDocs_version' );

        // seems like it's already installed
        if ( $version ) {
            return;
        }

        // skip if there's a page already with [ibxDocs] shortcode
        $pages_query = new WP_Query( array(
            'post_type'      => 'page',
            'posts_per_page' => -1,
            's'              => '[ibxDocs'
        ) );

        if ( $pages_query->found_posts ) {
            return;
        }

        $docs_page = wp_insert_post( array(
            'post_type'      => 'page',
            'post_title'     => 'Documentation',
            'post_author'    => get_current_user_id(),
            'post_content'   => '[ibxDocs]',
            'post_status'    => 'publish',
            'comment_status' => 'closed',
            'ping_status'    => 'closed',
            'post_name'      => 'docs',
        ) );

        if ( ! is_wp_error( $docs_page ) ) {
            $settings = get_option( 'ibxDocs_settings', array() );
            $settings['docs_home'] = $docs_page;

            update_option( 'ibxDocs_settings', $settings );
        }
    }

} // ibxDocs

/**
 * Initialize the plugin
 *
 * @return \ibxDocs
 */
function ibxDocs() {
    return ibxDocs::init();
}

// kick it off
ibxDocs();
